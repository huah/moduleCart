<!-- 
  Software Requirements Sepcification 
  Initiated by Gregory Vincic at Lapsystems 2002.

-->

<!ELEMENT srs          (project,nfrs,useCaseTree)>


<!-- 
  project
  Various project descriptions
-->

<!ELEMENT project          (title,version,dl)>



<!-- 
  nfrs = Non Functional RequirementS
  The dl tag is used to add an introduction or 
  other complementary information to this section.
-->

<!ELEMENT nfrs         (dl+)>


<!-- 
  useCaseTree
  The use case tree contains information about
  actors and their interactions with the system
  grouped in use cases.
  The dl tag is used to add an introduction or 
  other complementary information to this section.
-->

<!ELEMENT useCaseTree (dl?,actor+,useCase*)>


<!-- 
  useCase
  Each use case refers to one or more actors 
  with the actors attribute.
  Id should be written as U1,U2,U3,...,U?
  The actors attribute is a list of actor id's written
  as "A1 A2 .. A?".
-->

<!ELEMENT useCase (title,description?,(requirement|reqRef)*,action*)>
<!ATTLIST useCase 
       id         ID          #REQUIRED
       actors     IDREFS      #REQUIRED
       priority   (1|2|3|4|5) "3">


<!-- 
  actor
  Define which actors shall interact with the system
  in the beginning of your useCaseTree.
  Id should be written as A1,A2,A3,...,A?
-->

<!ELEMENT actor (name)>
<!ATTLIST actor        
       id         ID          #REQUIRED>


<!-- 
  requirement
  Each action or reaction may be restricted with
  one or more requirements.
  Id should be written as R1,R2,R3,...,R?
-->

<!ELEMENT requirement (description,(description|requirement|reqRef)*)>
<!ATTLIST requirement 
       id         ID          #REQUIRED
       status     (New|Rev|Val) #REQUIRED
       priority   (1|2|3|4|5) "3">


<!-- 
  reqRef = Requirement Reference
  A requirement defined elsewhere may be referenced
  with the reqRef element
  ids may be a list of requirements and shall be written as
  "R1 R3 R7".
-->

<!ELEMENT reqRef (#PCDATA)>
<!ATTLIST reqRef     
       ids        IDREFS      #REQUIRED>


<!-- 
  action, reaction
  Each useCase starts out with an action which
  is followed by one or more actions or reactions.
-->

<!ELEMENT action (description,(requirement|reqRef)*,(action|reaction)*)>

<!ELEMENT reaction (description,(requirement|reqRef)*,action*)>


<!-- 
  dl
  definition list
-->

<!ELEMENT dl     (dt,dd)*>
<!ELEMENT dt     (#PCDATA)>
<!ELEMENT dd     (#PCDATA|p|img|requirement|reqRef)*>


<!-- 
  These elements are sub elements to the above
-->

<!ELEMENT title        (#PCDATA)>

<!ELEMENT name         (#PCDATA)>

<!ELEMENT description  (#PCDATA|p|img)*>

<!ELEMENT p            (#PCDATA|img)*>

<!ELEMENT version      (#PCDATA)>

<!ELEMENT img (#PCDATA)>

<!ATTLIST img
    class    CDATA  #IMPLIED
    src      CDATA  #IMPLIED
    align    (center|left|right|middle) #IMPLIED>
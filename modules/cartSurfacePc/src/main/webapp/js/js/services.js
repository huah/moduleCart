/* Services */
var servs = angular.module('shopApp.services', ['ngResource']);

/************************************************************************************************
*
* General data services
*
************************************************************************************************/
// Get the most recently added products and show in main page
servs.factory('mainProductsList',function($resource){
	return $resource('rest/shop/products/last.json',{},{query:{method:'GET',params:{},isArray:true}});
});

servs.factory('mainPromoList',function($resource){
  return $resource('rest/shop/products/promos.json',{},{query:{method:'GET',params:{}, isArray:true}});
});

servs.factory('mainCategory',function($resource){
	  return $resource('rest/shop/categories/list.json',{},{query:{method:'GET',params:{}, isArray:true}});
});

servs.factory('uiConfigResource',function($resource){
	  return $resource('rest/shop/configuration/ui.json',{},{query:{method:'GET',params:{}, isArray:false}});
});
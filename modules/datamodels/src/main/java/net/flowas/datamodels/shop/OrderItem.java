package net.flowas.datamodels.shop;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.Data;

@Data
@Entity
public class OrderItem extends IdAndDate{
        @ManyToOne
	private ShoppingCart  order;
        @ManyToOne
	private Product product;
}
